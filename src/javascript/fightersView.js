import View from './view';
import FighterView from './fighterView';
import { fighterService } from './services/fightersService';
import { fight } from './fight';

class FightersView extends View {
  constructor(fighters) {
    super();
    
    this.handleClick = this.handleFighterClick.bind(this);
    this.createFighters(fighters);
  }

  fightersDetailsMap = new Map();
  activeModal = false;

  static rootElement = document.getElementById('root');

  createFighters(fighters) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, this.handleClick);
      return fighterView.element;
    });

    this.element = this.createElement({ tagName: 'div', className: 'fighters'});
    this.element.append(...fighterElements);
  }

  async handleFighterClick(event, fighter) {
    if(!this.activeModal) {
        let fighterDetails;

      // get from map or load info and add to fightersMap
      if (this.fightersDetailsMap.has(fighter._id)) {
        fighterDetails = this.fightersDetailsMap.get(fighter._id);
      } else {
        const info = await fighterService.getFighterDetails(fighter._id);
        this.fightersDetailsMap.set(fighter._id, info);
        fighterDetails = info;
      }

      // show modal with fighter info or hide if it was shown before
      const fighterElement = document.getElementById(`${fighter._id}`);
      const modalContent = this.createModal(fighterDetails);
    
      fighterElement.insertAdjacentElement('beforeEnd', modalContent);

      // allow to edit health and power in this modal (maximum should be set to 99)
      const modalItemsClasses = ['attack', 'defense', 'health'];
      const elements = [];

      modalItemsClasses.forEach((el,i) => {
        elements.push(document.getElementById(el));
        elements[i].addEventListener('input', (e) => {
          fighterDetails[el] = +e.target.value > 99 ? 99 : e.target.value;
          elements[i].value = fighterDetails[el]; 
        });
      });

      this.activeModal = true
      event.target.parentNode.classList.toggle("chosen");
      this.chosenFighters(fighterElement, fighter._id);
      
    } else {
      const elem = document.querySelector('.modal');
      elem.parentNode.removeChild(elem);
      this.activeModal = false;
    }
  }

  createModal ({attack, defense, health}) {
    const modalElement = this.createElement({ tagName: 'div', className: 'modal' });
    const modalDOM = 
    `   
      <label for="attack" class="attack__label">🗡️</label>
      <input type="number"  value = ${attack} class="model__input attack" placeholder="attack" id="attack" required>
      <label for="defense" class="defense__label">🛡️</label>
      <input type="number"  value = ${defense} class="model__input defense" placeholder="defense" id="defense" required>
      <label for="health" class="health__label">❤️</label>
      <input type="number"  value = ${health} class="model__input health" placeholder="health" id="health" required>
        
    `;
    modalElement.innerHTML = modalDOM
    return modalElement;
  }

  createStartBtn (firstFighter, secondFighter) {
    const modalElement = this.createElement({ tagName: 'button', className: 'btn', attributes: {id:'start'} });

    modalElement.innerHTML = 'FIGHT';
    FightersView.rootElement.appendChild(modalElement);
    modalElement.addEventListener('click', (e) => {
      fight(firstFighter, secondFighter);
    })
  }

  removeStartBtn (startBtn) {
    startBtn.parentNode.removeChild(startBtn);
    FightersView.rootElement.insertAdjacentHTML('beforeend','<span class="info_label">Choose two fighters</span>');
  }

  chosenFighters () {
    const chosenFightersList = document.querySelectorAll('.chosen');
    const infoLabele = document.querySelector('.info_label');
    const startBtn = document.getElementById('start');

    if(chosenFightersList.length === 2) {
      const firstFighter = this.fightersDetailsMap.get(chosenFightersList[0].id);
      const secondFighter = this.fightersDetailsMap.get(chosenFightersList[1].id);
      infoLabele.parentNode.removeChild(infoLabele);
      this.createStartBtn(firstFighter, secondFighter);
    } else if (chosenFightersList.length > 2) {
      this.removeStartBtn(startBtn);
    } else if (chosenFightersList.length < 2 && startBtn) {
      this.removeStartBtn(startBtn);
    }
  }
}

export default FightersView;