class Fighter {
  constructor({name, health, attack, defense, source, _id}) {
    this.name = name;
    this.health = +health;
    this.attack = +attack;
    this.defense = +defense;
    this.source = source;
    this._id = _id;
  }

  getHitPower() {
    let criticalHitChance = Math.floor(Math.random() * 2) + 1;
    let power = this.attack * criticalHitChance;
    return power;
  }

  getBlockPower () {
    let dodgeChance = Math.floor(Math.random() * 2) + 1;
    let defensePower = this.defense * dodgeChance;
    return defensePower;
  }
}

export default Fighter;