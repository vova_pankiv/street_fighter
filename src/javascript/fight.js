import Fighter from './Fighter';
import FighterView from './fighterView';
import App from './app';

const rootDOM = document.getElementById('root');

export const fight =  (...fighters) {
  rootDOM.innerHTML = '';
  rootDOM.style.flexDirection = 'row';

  // Create instance of current fighters
  const currentFighters = [];
  fighters.forEach(el => currentFighters.push(new Fighter (el)));
  
  currentFighters.map((fighter, i) => {
    const chosenFighter = new FighterView(fighter, attackHandler).element;

    // Create UI elements for attack
    const attackFirstFighter = `
    <button class="attack__btn" id=${fighter._id}>Attack</button>
  `;

    // Insert fighters and attack buttons into DOM
    chosenFighter.insertAdjacentHTML('beforeend', attackFirstFighter);
    rootDOM.append(chosenFighter);

    // Add UI elements for health of fighters
    const healthFirstFighter = `
    <div>
      <div class="health__bar" id='health-${fighter._id}'>${fighter.health}</div>
    </div>
    `;
    chosenFighter.insertAdjacentHTML('afterbegin', healthFirstFighter);
    
  });

  // Add eventListener for attack button
  document.getElementById(`${currentFighters[0]._id}`).addEventListener('click', event => handleAttack(event, currentFighters[0], currentFighters[1]));
  document.getElementById(`${currentFighters[1]._id}`).addEventListener('click', event => handleAttack(event, currentFighters[1], currentFighters[0]));
}

function handleAttack (event, attacker, defender) {
  // Calculate iron of attack
  const attack = attacker.getHitPower();
  const defence = defender.getBlockPower();
  const iron = (attack - defence) > 0 ? attack - defence : 0;

  // Calculate and update UI of defender health property
  defender.health = defender.health - iron;
  updateHealth(defender._id, defender.health);

  // Audio effect of hit
  window.audio = new Audio ('./resources/sounds/punch.mp3');
  window.audio.play();

  // End of game
  if(defender.health <= 0) {
    alert(`Player ${attacker.name} won the battle!`)
    rootDOM.innerHTML = '';
    rootDOM.style.flexDirection = 'column';
    new App();
  }
}

function updateHealth (playerId, health) {
  const healthBarFirst = document.getElementById(`health-${playerId}`);
  healthBarFirst.innerHTML = health;
  healthBarFirst.style.width = health +'%';
  if (health < 20) {
    healthBarFirst.style.backgroundColor = 'red';
  }
}

function attackHandler () {
  window.audio = new Audio ('./resources/sounds/punch.mp3');
  window.audio.play();
}
